import numpy as np
import matplotlib.pyplot as plt
import time
import math

"""
Aqui resolveremos o problema de 2 corpos, dadas as condições iniciais
r1, r2, p1, p2, m1, m2
"""
def CM():
    return (m1*r1 + m2*r2) / (m1 + m2)

def stability(r1, r2, m1, m2):
    '''
    Analisa a estabilidade do nosso método pelas 
        grandezas físicas energia total e momento angular.
    '''    
    dist = np.linalg.norm(r1 - r2)
    U = - G * m1 * m2 / (dist + epsilon)
    Ek = np.linalg.norm(p1) ** 2 / (m1 * 2.) + np.linalg.norm(p2) ** 2 / (m2 * 2.)  # Ek = 1/2 * mv^2
    Et = Ek + U
    
    nr1 = np.linalg.norm(r1)
    np1 = np.linalg.norm(p1)
    theta1 = math.acos(np.dot(r1 / nr1 , p1 / np1))
    l1 = nr1 * np1 * math.sin(theta1)

    nr2 = np.linalg.norm(r2)
    np2 = np.linalg.norm(p2)
    theta2 = math.acos(np.dot(r2 / nr2 , p2 / np2))
    l2 = nr2 * np2 * math.sin(theta2)

    L = l1 + l2
    
    return Et, L

def force(r1, r2, m1, m2):
    '''
    Retorna a força gravitacional que 2 causa em 1
    (1)---->   (2)
    '''
    r12 = r2 - r1
    dist = np.linalg.norm(r12)

    return (G * m1 * m2 / (epsilon + dist ** 3)) * r12

def euler(p, r, m, F, dt):
    '''
    Usa método de Euler para atualizar a posição
        das partículas.
    '''
    p += F * dt  # 2a Lei de Newton: F*dt = dp 
    r += p / m * dt # equação problema: d2r/dt2 = Gm/r^2
    return p, r

G = 5  #cte de Gravitaçao Universal (proporcionalidade)
epsilon = 1.E-8 #cte para estabilidade
# condições iniciais:
r1 = np.array([0., 0.]) # vetor posição [x, y]
r2 = np.array([100., 50.])

p1 = np.array([10., 0.]) # vetor momento [px, py]
p2 = np.array([0., 0.])

m1 = 10
m2 = 10

# Centralizar simulação no centro de massa
rcm = CM()

r1 -= rcm
r2 -= rcm

vcm = (p1 + p2) / (m1 + m2)

p1 -= m1 * vcm
p2 -= m2 * vcm

# Pedimos ao usurário
tf = float(input("duração da simulação (seg): ")) 
t = 0.
N = int(input("número de iterações: "))
delta_t = tf/N  # timestep
print("timestep =", delta_t)
metodo = int(input("escolha o método: "))

file = open('output_2bp.txt', 'w')

stopwatch = time.time() # para análise de desempenho

# Escreve em output a Duração, o número de Iterações e o método
while t < tf: # enquanto durar a simulação
    F = force(r1, r2, m1, m2)

    # Atualiza as posições dos dois objetos
    # Método 1: Euler
    if metodo == 1:
        p1, r1 = euler(p1, r1, m1, F, delta_t)
        p2, r2 = euler(p2, r2, m2,-F, delta_t)  # 3a Lei de Newton: F12 = -F21
    
    # Método 2: Euler modificado (Runge-Kutta, O[2])
    if metodo == 2:
        kp1, kr1 = euler(p1, r1, m1, F, delta_t)
        kp2, kr2 = euler(p2, r2, m2,-F, delta_t)
        # passo de aproximação de euler:
        F = force(kr1, kr2, m1, m2)
        # no euler modificado, faz-se uma média:
        # Uma maneira de se fazer a conta usando a função euler()
        p1 = 0.5 * (euler(p1, r1, m1, F, delta_t)[0] + kp1)
        p2 = 0.5 * (euler(p2, r2, m2,-F, delta_t)[0] + kp2)
        r1 = 0.5 * (euler(p1, r1, m1, F, delta_t)[1] + kr1)
        r2 = 0.5 * (euler(p2, r2, m2,-F, delta_t)[1] + kr2)
        # A outra maneira encontra-se abaixo
        '''
        p1 += 0.5 * ( F * delta_t + kp1)
        r1 += 0.5 * (p1/m1 * delta_t + kr1)
        p2 += 0.5 * (-F * delta_t + kp2)
        r2 += 0.5 * (p2/m2 * delta_t + kr2)
        '''
    # Método 3: Runge-Kutta de ordem 4
    # o método abaixo ficou um pouco confuso, mas deve funcionar como o Runge-Kutta padrão...
    if metodo == 3:
        # kXpY em que X é a ordem do k do método e Y refere-se à partícula.
        k1p1 =  F  # dp/dt = F; no nosso caso, a força grav é a "função f"
        k1r1 = (p1/m1) # k de primeira ordem da posição (r) da partícula 1. #kr's ainda estranhos, olhar
        k1p2 = -F
        k1r2 = (p2/m2)

        k2p1 =  force(r1+(0.5)*delta_t*k1r1, r2+(0.5)*delta_t*k1r2, m1, m2)
        k2r1 = (p1 + k2p1*(0.5)*(delta_t)) / m1
        k2p2 = -force(r1+(0.5)*delta_t*k1r1, r2+(0.5)*delta_t*k1r2, m1, m2)
        k2r2 = (p2 + k2p2*(0.5)*(delta_t)) / m2

        k3p1 =  force(r1+(0.5)*delta_t*k2r1, r2+(0.5)*delta_t*k2r2, m1, m2)
        k3r1 = (p1 + k3p1*(0.5)*(delta_t)) / m1
        k3p2 = -force(r1+(0.5)*delta_t*k2r1, r2+(0.5)*delta_t*k2r2, m1, m2)
        k3r2 = (p2 + k3p2*(0.5)*(delta_t)) / m2

        k4p1 =  force(r1+k3r1*delta_t, r2+k3r2*delta_t, m1, m2)
        k4r1 = (p1 + k4p1*(delta_t))/m1
        k4p2 = -force(r1+k3r1*delta_t, r2+k3r2*delta_t, m1, m2)
        k4r2 = (p2 + k4p2*(delta_t))/m2
        
        p1 += delta_t/6. * (k1p1 + 2*k2p1 + 2*k3p1 + k4p1)
        r1 += delta_t/6. * (k1r1 + 2*k2r1 + 2*k3r1 + k4r1) 
        p2 += delta_t/6. * (k1p2 + 2*k2p2 + 2*k3p2 + k4p2)
        r2 += delta_t/6. * (k1r2 + 2*k2r2 + 2*k3r2 + k4r2) 
        
    Et, L = stability(r1,r2,m1,m2)

    file.write('%f %f %f %f %f %f %f\n' %(t, Et, L, r1[0], r1[1], r2[0], r2[1])) # imprime t, Et, L, r1, r2
    
    t += delta_t

print("elapsed time(sec): ", time.time()-stopwatch)
file.close()
