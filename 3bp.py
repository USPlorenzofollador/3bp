import numpy as np
import matplotlib.pyplot as plt
import time
import math
import matplotlib.animation as animation
from matplotlib import rc
"""
Aqui resolveremos o problema de 3 corpos, dadas as condições iniciais
r1, r2, r3, p1, p2, p3, m1, m2, m3
"""
def CM():
    return (m1*r1 + m2*r2 + m3*r3) / (m1 + m2 + m3)

def stability():
    '''
    Analisa a estabilidade do nosso método pelas 
        grandezas físicas energia total e momento angular.
    '''
    G = 5  #cte de Gravitaçao Universal (proporcionalidade)
    epsilon = 1.E-8 #cte para estabilidade
    
    dist12 = np.linalg.norm(r1 - r2) + epsilon
    dist13 = np.linalg.norm(r1 - r3) + epsilon
    dist23 = np.linalg.norm(r2 - r3) + epsilon
    U = - G * (m1 * m2 / (dist12 + epsilon) + m1 * m3 / (dist13 + epsilon) + m2 * m3 / (dist23 + epsilon))
    Ek = np.linalg.norm(p1) ** 2 / (m1 * 2.) + np.linalg.norm(p2) ** 2 / (m2 * 2.) + np.linalg.norm(p3) ** 2 / (m3 * 2.)  # Ek = 1/2 * mv^2
    Et = Ek + U
    
    nr1 = np.linalg.norm(r1) + epsilon
    np1 = np.linalg.norm(p1) + epsilon
    theta1 = math.acos(np.dot(r1 / nr1 , p1 / np1))
    l1 = nr1 * np1 * math.sin(theta1)

    nr2 = np.linalg.norm(r2) + epsilon
    np2 = np.linalg.norm(p2) + epsilon
    theta2 = math.acos(np.dot(r2 / nr2 , p2 / np2))
    l2 = nr2 * np2 * math.sin(theta2)
    
    nr3 = np.linalg.norm(r3) + epsilon
    np3 = np.linalg.norm(p3) + epsilon
    theta3 = math.acos(np.dot(r3 / nr3 , p3 / np3))
    l3 = nr3 * np3 * math.sin(theta3)

    L = l1 + l2 + l3
    
    return Et, L

def force(ra, rb, ma, mb):#pronto
    '''
    Retorna a força gravitacional que 2 causa em 1
    (1)---->   (2)
    '''
    G = 5  #cte de Gravitaçao Universal (proporcionalidade)
    epsilon = 1.E-8 #cte para estabilidade
    
    rab = rb - ra
    dist = np.linalg.norm(rab)

    return (G * ma * mb / (epsilon + dist ** 3)) * rab

# MÉTODOS NUMÉRICOS

def euler(p, r, m, F, dt):
	'''
	Usa método de Euler para atualizar a posição
		das partículas.
	'''
	p += F * dt  # 2a Lei de Newton: F*dt = dp 
	r += p / m * dt # equação problema: d2r/dt2 = Gm/r^2
	return p, r


# condições iniciais:
r1 = np.array([30., 50.]) # vetor posição [x, y, z]
r2 = np.array([100., 50.])
r3 = np.array([50., 100.])

p1 = np.array([5., 0.]) # vetor momento [px, py]
p2 = np.array([0., 10.])
p3 = np.array([-3., -4.])

m1 = 10
m2 = 10
m3 = 10

# Centralizar simulação no centro de massa
rcm = CM()

r1 -= rcm
r2 -= rcm
r3 -= rcm

vcm = (p1 + p2 + p3) / (m1 + m2 + m3)

p1 -= m1 * vcm
p2 -= m2 * vcm
p3 -= m3 * vcm

# Pedimos ao usurário
tf = float(input("duração da simulação (seg): ")) 
t = 0.
N = int(input("número de iterações: "))
delta_t = tf/N  # timestep
print("timestep =", delta_t)
metodo = int(input("escolha o método: "))

file = open('output_3bp.txt', 'w')

stopwatch = time.time() # para análise de desempenho

    # Escreve em output a Duração, o número de Iterações e o método
while t < tf: # enquanto durar a simulação
    F21 = force(r1, r2, m1, m2)
    F31 = force(r1, r3, m1, m3)
    F32 = force(r2, r3, m2, m3)
    F1 =  F21 + F31
    F2 = -F21 + F32
    F3 = -F31 - F32
    # Atualiza as posições dos dois objetos
    # Método 1: Euler
    if metodo == 1:
        p1, r1 = euler(p1, r1, m1, F1, delta_t)
        p2, r2 = euler(p2, r2, m2, F2, delta_t)  # 3a Lei de Newton: F12 = -F21
        p3, r3 = euler(p3, r3, m3, F3, delta_t)
    # Método 2: Euler modificado (Runge-Kutta, O[2])
    if metodo == 2:
        kp1, kr1 = euler(p1, r1, m1, F1, delta_t)
        kp2, kr2 = euler(p2, r2, m2, F2, delta_t)
        kp3, kr3 = euler(p3, r3, m3, F3, delta_t)
        # passo de aproximação de euler:
        F21 = force(kr1, kr2, m1, m2)
        F31 = force(kr1, kr3, m1, m3)
        F32 = force(kr2, kr3, m2, m3)
        # no euler modificado, faz-se uma média:
        F1 =  F21 + F31
        F2 = -F21 + F32  # 3a Lei de Newton: F12 = -F21
        F3 = -F31 - F32
        # Uma maneira de se fazer a conta usando a função euler()
        p1 = 0.5 * (euler(p1, r1, m1, F1, delta_t)[0] + kp1)
        p2 = 0.5 * (euler(p2, r2, m2, F2, delta_t)[0] + kp2)
        p3 = 0.5 * (euler(p3, r3, m3, F3, delta_t)[0] + kp3)
        r1 = 0.5 * (euler(p1, r1, m1, F1, delta_t)[1] + kr1)
        r2 = 0.5 * (euler(p2, r2, m2, F2, delta_t)[1] + kr2)
        r3 = 0.5 * (euler(p3, r3, m3, F3, delta_t)[1] + kr3)
    if metodo == 3:
        # kXpY em que X é a ordem k de 'estagio' do método e Y refere-se à partícula.
        k1p1 = F1  # dp/dt = F; no nosso caso, a força grav é a "função f"
        k1r1 = (p1/m1) # k de primeira ordem da posição (r) da partícula 1. #kr's ainda estranhos, olhar
        k1p2 = F2
        k1r2 = (p2/m2)
        k1p3 = F3
        k1r3 = (p3/m3)

        k2p1 = force(r1 + (0.5)*delta_t*k1r1, r2 + (0.5)*delta_t*k1r2, m1, m2) + force(r1 + (0.5)*delta_t*k1r1, r3 + (0.5)*delta_t*k1r3, m1, m3)
        k2r1 = (p1 + k2p1*(0.5)*(delta_t))/m1
        k2p2 = -force(r1 + (0.5)*delta_t*k1r1, r2 + (0.5)*delta_t*k1r2, m1, m2) + force(r2 + (0.5)*delta_t*k1r2, r3 + (0.5)*delta_t*k1r3, m2, m3)
        k2r2 = (p2 + k2p2*(0.5)*(delta_t))/m2
        k2p3 = -force(r1 + (0.5)*delta_t*k1r1, r3 + (0.5)*delta_t*k1r3, m1, m3) - force(r2 + (0.5)*delta_t*k1r2, r3 + (0.5)*delta_t*k1r3, m2, m3)
        k2r3 = (p3 + k2p3*(0.5)*(delta_t))/m3

        k3p1 = force(r1 + (0.5)*delta_t*k2r1, r2 + (0.5)*delta_t*k2r2, m1, m2) + force(r1 + (0.5)*delta_t*k2r1, r3 + (0.5)*delta_t*k2r3, m1, m3)
        k3r1 = (p1 + k3p1*(0.5)*(delta_t))/m1
        k3p2 = -force(r1 + (0.5)*delta_t*k2r1, r2 + (0.5)*delta_t*k2r2, m1, m2) + force(r2 + (0.5)*delta_t*k2r2, r3 + (0.5)*delta_t*k2r3, m2, m3)
        k3r2 = (p2 + k3p2*(0.5)*(delta_t))/m2
        k3p3 = -force(r1 + (0.5)*delta_t*k2r1, r3 + (0.5)*delta_t*k2r3, m1, m3) - force(r2 + (0.5)*delta_t*k2r2, r3 + (0.5)*delta_t*k2r3, m2, m3)
        k3r3 = (p3 + k3p3*(0.5)*(delta_t))/m3

        k4p1 = force(r1 + delta_t*k3r1, r2 + delta_t*k3r2, m1, m2) + force(r1 + delta_t*k3r1, r3 + delta_t*k3r3, m1, m3)
        k4r1 = (p1 + k4p1*(delta_t))/m1
        k4p2 = -force(r1 + delta_t*k3r1, r2 + delta_t*k3r2, m1, m2) + force(r2 + delta_t*k3r2, r3 + delta_t*k3r3, m2, m3)
        k4r2 = (p2 + k4p2*(delta_t))/m2
        k4p3 = -force(r1 + delta_t*k3r1, r3 + delta_t*k3r3, m1, m3) - force(r2 + delta_t*k3r2, r3 + delta_t*k3r3, m2, m3)
        k4r3 = (p3 + k4p3*(delta_t))/m3
        
        p1 += (delta_t/6.) * (k1p1 + 2*k2p1 + 2*k3p1 + k4p1)
        r1 += (delta_t/6.) * (k1r1 + 2*k2r1 + 2*k3r1 + k4r1) 
        p2 += (delta_t/6.) * (k1p2 + 2*k2p2 + 2*k3p2 + k4p2)
        r2 += (delta_t/6.) * (k1r2 + 2*k2r2 + 2*k3r2 + k4r2) 
        p3 += (delta_t/6.) * (k1p3 + 2*k2p3 + 2*k3p3 + k4p3)
        r3 += (delta_t/6.) * (k1r3 + 2*k2r3 + 2*k3r3 + k4r3)
    
    Et, L = stability()
    file.write('%f %f %f %f %f %f %f %f %f\n' %(t, Et, L, r1[0], r1[1], r2[0], r2[1], r3[0], r3[1])) # imprime t, Et, L, r1, r2
    t += delta_t
print("elapsed time(sec): ", time.time()-stopwatch)
file.close()