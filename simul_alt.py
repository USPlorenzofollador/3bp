"""
IDEIA: usar apenas para rodar o gráfico, já que vamos só apresentar os vídeos
 já prontos
"""

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
from matplotlib import rc

# carregar variáveis
file = "output_2bp.txt"
paths = np.loadtxt(file)

# Propriedades do sistema
t = paths[:,0]
Et= paths[:,1]
L = paths[:,2]

# Posições das partículas
x = paths[:,[3,5]]
y = paths[:,[4,6]]

fig = plt.figure()

left = 0.15
width = 0.7

ax1 = plt.axes([left, 0.25, width, width])
ax2 = plt.axes([left, 0.1, width, 0.1])

mat, = ax1.plot([], [], 'o', color='blue')


# toma o número de frames da animação a partir do tamanho do arquivo
frames = paths.shape[0]

def init():
    mat.set_data([], [])
    
    ax2.set_xlabel('time(s)')
    ax2.set_ylabel('E(t)')
    
    ax1.set_xlim((np.min(x), np.max(x)))
    ax1.set_ylim((np.min(y), np.max(y)))
    
    ax2.set_xlim((t[0], t[-1]))
    ax2.set_ylim((100, 300))
    
    ax2.scatter(t[:], L[:], marker='o', color='r', s=.01, alpha=0.01)
    return mat,

def animate(i):
    mat.set_data(x[i], y[i]) 
    ax2.scatter(t[:i], L[:i], marker='o', color='r', s=1)
    
    return mat,

anim = animation.FuncAnimation(fig, animate, init_func=init,
                               frames=frames, interval=2, blit=True)
plt.show()
