import numpy as np
import matplotlib.pyplot as plt

# carregar variáveis
file  = "output_2bp.txt"
paths = np.loadtxt(file)

# posições das partículas
x = paths[:,[3,5]]
y = paths[:,[4,6]]

fig = plt.figure()
ax  = plt.axes(xlim=(np.min(x), np.max(x)), ylim=(np.min(y), np.max(y)))

trajectory = ax.plot(x, y, '-', color='blue')
cm = ax.plot(0., 0., 'x', color='grey')
plt.show()
