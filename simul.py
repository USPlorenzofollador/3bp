import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
from matplotlib import rc

# carregar variáveis
file = "output_2bp.txt"
paths = np.loadtxt(file)

# Propriedades do sistema
t = paths[:,0]
Et= paths[:,1]
L = paths[:,2]

# Posições das partículas
x = paths[:,[3,5]]
y = paths[:,[4,6]]

# inicia a figura e os objetos
fig = plt.figure()
ax = plt.axes(xlim=(np.min(x), np.max(x)), ylim=(np.min(y), np.max(y)))

mat, = ax.plot([], [], 'o', color='blue')
cm,  = ax.plot([], [], 'x', color='grey')
trace1, = ax.plot([], [], ',-', lw=1, color = 'orange')
trace2, = ax.plot([], [], ',-', lw=1, color = 'green')

# Imprimir os textos na animação:
t_template = 't = %.1fs'
t_text = ax.text(0.05, 0.9, '', transform=ax.transAxes)

Et_template = 'Et = %.3fJ'
Et_text = ax.text(0.05, .86, '', transform=ax.transAxes)

L_template = r'L = %.3f$kgm^2/s$'
L_text = ax.text(0.05, 0.82, '', transform=ax.transAxes)

# toma o número de frames da animação a partir do tamanho do arquivo
frames = paths.shape[0]

history_x1, history_y1 = [], []
history_x2, history_y2 = [], []
line, = ax.plot([], [], 'o-', lw=1)

def init():
    mat.set_data([], [])
    cm.set_data(0,0)
    trace1.set_data([],[])
    trace2.set_data([],[])
    return mat, cm, trace1, trace2

def animate(i):
    pos_x = x[i]
    pos_y = y[i]
    mat.set_data(pos_x, pos_y) 
    cm.set_data(0,0)
    
    history_x1.append(pos_x[0])
    history_y1.append(pos_y[0])
    
    history_x2.append(pos_x[1])
    history_y2.append(pos_y[1])
    
    trace1.set_data(history_x1, history_y1)
    trace2.set_data(history_x2, history_y2)
    
    line.set_data(pos_x, pos_y)
    
    # Texto da animação
    t_text.set_text(t_template % (t[i] * 1000))
    Et_text.set_text(Et_template % (Et[i]))
    L_text.set_text(L_template % (L[i]))
    
    return mat, cm, line, trace1, trace2, t_text, Et_text, L_text

anim = animation.FuncAnimation(fig, animate, init_func=init,
                               frames=frames, interval=2, blit=True)
#anim.save('simul.gif', writer='imagemagick', fps=15)
plt.show()
