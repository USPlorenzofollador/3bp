import numpy as np
import matplotlib.pyplot as plt

# carregar variáveis
file  = "output_3bp.txt"
paths = np.loadtxt(file)

# posições das partículas
x = paths[:,[3,5,7]]
y = paths[:,[4,6,8]]

plt.plot(x,y)
plt.show()
