## Solução Numérica do Problema de 3 Corpos

O problema de 3 corpos é um exemplo clássico de aplicação de EDOs (de segunda ordem), que não possui solução analítica. Dessa forma, as posições dos corpos - sujeitos apenas à mútua atração gravitacional - precisam ser calculadas numericamente, e programas computacionais são perfeitos para essa tarefa.

O projeto foi desenvolvido progressivamente e apresenta diversos programas ilustrativos antes solução mais geral.

----

#### 2bp.py
Esse programa calcula as posições do problema de dois corpos, e os parâmetros iniciais estão escritos no próprio código. O programa pergunta ao usuário a duração (em segundos) da simulação, o número de iterações (que determina o tamanho do passo) e o método de cálculo. Atualmente, o programa calcula com o método de Euler e o método modificado de Euler (Runge-Kutta simples). O arquivo `parameters.txt` armazena essas informações.

Como saída, o programa escreve o arquivo `output_2bp.txt`, que tem a seguinte formatação:
```
tempo   | energia | mom. ang. |    x1   |   y1     |   x2    |   y2

0.000000 -1.972553 250.000000 -49.496422 -24.998211 49.496422 24.998211
1.000000 -1.972985 250.000000 -48.989216 -24.994590 48.989216 24.994590
2.000000 -1.973434 250.000000 -48.478328 -24.989090 48.478328 24.989090
3.000000 -1.973899 250.000000 -47.963705 -24.981665 47.963705 24.981665
4.000000 -1.974382 250.000000 -47.445292 -24.972265 47.445292 24.972265
5.000000 -1.974883 250.000000 -46.923030 -24.960840 46.923030 24.960840
6.000000 -1.975404 250.000000 -46.396862 -24.947337 46.396862 24.947337
7.000000 -1.975946 250.000000 -45.866726 -24.931701 45.866726 24.931701
8.000000 -1.976509 250.000000 -45.332561 -24.913875 45.332561 24.913875
9.000000 -1.977096 250.000000 -44.794302 -24.893798 44.794302 24.893798
```

----

#### simul.py
Esse programa recebe como entrada os parâmetros e as saidas de `2bp.py` e mostra na tela uma animação representativa das coordenadas dos corpos conforme eles interagem gravitacionalmente.

----

#### simul_alt.py
(INACABADO) Semelhante `2bp.py`, porém também anima um gráfico das energias abaixo da janela da simulação.
(IDEIA) Poderíamos usar este programa para animar apenas o gráfico e inserir o vídeo em baixo na apresentação.

----

#### 3bp.py
Semelhante ao `2bp.py`, porém com 3 corpos e em 3 dimensões.

----

#### simul3bp.py
Semelhante ao `simul.py`, porém apresentando a animação da saída de `3bp.py`.

